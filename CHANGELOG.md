- 1.5.1 (5 Dec. 2024): Marked as compatible with 1.21.4
- 1.5.0 (18 Nov. 2024):
  - Updated for 1.21.2-1.21.3
  - Add descriptions for the Mace-exclusive enchantments: Breach, Density, and Wind Burst
- 1.4.1 (28 Aug. 2024): Marked as compatible with 1.21.1
- 1.4.0 (12 Jul. 2024):
  - Updated for 1.21!
  - Fixed a bug with server-side line wrapping that resulted in missing pages
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates (client-side)
  - Minor internal changes
- 1.3.6 (8 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.3.5 (24 Apr. 2024): Updated for 1.20.5
- 1.3.4 (28 Jan. 2024): Updated for 1.20.3 and 1.20.4
- 1.3.3 (30 Oct. 2023): Updated for 1.20.2
- 1.3.2 (14 Jun. 2023):
  - Updated for 1.20 and 1.20.1!
  - Server Translations is now bundled, no need to install it separately on servers (no change for client/singleplayer)
  - Fixed an issue where clicking the 'Take Book' button of enchanted book screens on a server would clear the book
  until reopening it
- 1.3.1 (5 Apr. 2023): Updated for 1.19.3 and 1.19.4
- 1.3.0-b1 (11 Aug. 2022): Added server-side support!
  - Client- and server-side functionality are independent, so clients with the mod can (still) connect to servers
  without it, and clients without it can connect to servers with it.
  - When on a server, [Server Translation API](https://www.curseforge.com/minecraft/mc-mods/server-translation-api) must be installed for the mod to function.
  - If the mod is installed on both client and server (not recommended), server translations will override client
  translations.
  - Page wrapping is not as precise when the mod is server-side. This can't be helped.
- 1.2.10 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.2.9 (28 Jul. 2022): Added lore (by [sailKite](https://github.com/sailKiteV)) and description for Swift Sneak
- 1.2.8 (27 Jul. 2022): Marked as compatible with 1.19.1
- 1.2.7 (22 Jun. 2022): Updated for 1.19!
- 1.2.6-1 (15 Mar. 2022): Now bundles the correct version of crowdin translate, fixing startup crash. Sorry about that.
- 1.2.6 (15 Mar. 2022):
  - Lower minimum required version for Fabric API
  - Now bundles Crowdin Translate again
- 1.2.5 (4 Mar. 2022):
  - Updated for 1.18.2
  - Removed CrowdinTranslate for now as it doesn't work on 1.18.2 yet.
- 1.2.4 (8 Jan. 2022):
  - Added description and lore for the Vein Mining enchantment added by the [Vein Mining](https://www.curseforge.com/minecraft/mc-mods/vein-mining-fabric) (Fabric) mod.
  - These were created by [xKuraion](https://www.curseforge.com/members/xkuraion), who also created the [Forge Port](https://www.curseforge.com/minecraft/mc-mods/enchantment-lore-forge)!
  - Added enchantment descriptions from [More Enchantment Descriptions (for Enchantment Lore)](https://www.curseforge.com/minecraft/texture-packs/more-enchantment-descriptions-for-enchantment-lore) by [TeaJ4y](https://www.curseforge.com/members/teaj4y).  
  This includes descriptions for enchantments from 
  [Mo'Enchantments](https://www.curseforge.com/minecraft/mc-mods/fabric-more-enchantments), 
  [Fabric Enchantments](https://www.curseforge.com/minecraft/mc-mods/fabric-enchantments), 
  [Charm](https://www.curseforge.com/minecraft/mc-mods/charm), 
  [Better End](https://www.curseforge.com/minecraft/mc-mods/betterend), and
  [Monster of the Ocean Depths](https://www.curseforge.com/minecraft/mc-mods/monster-of-the-ocean-depths-fabric).
- 1.2.3 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.2.2 (1 Dec. 2021): Updated for 1.18!
- 1.2.1 (13 Aug. 2021): No longer prevents some items' right-click functionality. Sorry about that. 
- 1.2 (10 Aug. 2021): 
  - Finally compatible with 1.17.1!
  - Now uses a Fabric API event instead of a mixin, which theoretically improves compatibility with other mods. 
  - Fixed a mistake in the description for Depth Strider
  - Fixed crowdin translate initialization
  - Removed some unused dependencies
- 1.1.1 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.1 (21 Dec. 2020): Initial release. 
  Added lore entries for all enchantments, thanks to [sailKite](https://github.com/sailKiteV) for contributing these!
- 1.0 (17 Dec. 2020): Initial version. 