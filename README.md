<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Enchantment Lore

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_430219_all.svg)](https://modrinth.com/mod/enchantment-lore/versions#all-versions)
![environment: any](https://img.shields.io/badge/environment-any-707070)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)
[![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety/enchantment_lore/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/enchantment_lore)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/enchantment_lore?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/enchantment_lore/-/issues)
[![localized: Percentage](https://badges.crowdin.net/enchantment-lore/localized.svg)](https://crwd.in/enchantment-lore)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/enchantment-lore?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/enchantment-lore/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/430219?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/enchantment-lore/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Open enchanted books like written books to read their description and lore.

Works client-side, server-side and in single player.  
It needn't be installed on both client and server.

[tinytransfem](https://www.curseforge.com/members/tinytransfem) has also created a
[Forge port](https://www.curseforge.com/minecraft/mc-mods/enchantment-lore-forge).

With this mod you'll be able to open and read enchanted books.  
For each enchantment in the book, there will be some lore and a description of how the enchantment works.

---

<details>

<summary>Mod integrations</summary>

[Enchantments Encore](https://modrinth.com/datapack/enchantments-encore) adds Enchantment Lore descriptions for its
enchantments as of version 2.4.

Enchantment Lore version 1.2.4+ adds descriptions for enchantments from:
- [Mo'Enchantments](https://www.curseforge.com/minecraft/mc-mods/fabric-more-enchantments)
- [Fabric Enchantments](https://www.curseforge.com/minecraft/mc-mods/fabric-enchantments)
- [Charm](https://www.curseforge.com/minecraft/mc-mods/charm)
- [Better End](https://www.curseforge.com/minecraft/mc-mods/betterend)
- [Monster of the Ocean Depths](https://www.curseforge.com/minecraft/mc-mods/monster-of-the-ocean-depths-fabric)

These are from
[More Enchantment Descriptions (for Enchantment Lore)](https://www.curseforge.com/minecraft/texture-packs/more-enchantment-descriptions-for-enchantment-lore)
by [TeaJ4y](https://www.curseforge.com/members/teaj4y).
The resource pack can still add these descriptions to older versions of the mod.

</details>

---

Lore and description entries can be added or changed using resource packs, learn how on
[the wiki](https://gitlab.com/supersaiyansubtlety/enchantment_lore/-/wikis/Home).  
I've made a few very basic ones that can be found
[here](https://gitlab.com/supersaiyansubtlety/enchantment_lore/-/tree/master/resourcepacks).

If you create and publish a resource pack that adds or modifies enchantment descriptions, feel free to message me and
I'll add a link to it here.

Mod showcase by [Boodlyneck](https://www.curseforge.com/members/boodlyneck/followers):
[starts at 2:09](https://www.youtube.com/watch?v=jZiiFddysZc&t=129s)

---

<details>

<summary>Translations</summary>

[![localized: Percentage](https://badges.crowdin.net/enchantment-lore/localized.svg)](https://crwd.in/enchantment-lore) [![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
Install [SSS Translate](https://modrinth.com/mod/sss-translate) to automatically download new translations (client-side only).  
You can help translate Enchantment Lore on [Crowdin](https://crwd.in/enchantment-lore).

</details>

---

<details>

<summary>Credits</summary>

- [sailKite](https://github.com/sailKiteV) for contributing the built-in lore entries!

- [thecatcore](https://github.com/thecatcore), [Patbox](https://github.com/Patbox), and all the
[contributors](https://github.com/NucleoidMC/Server-Translations/graphs/contributors) to
[Server-Translations](https://github.com/NucleoidMC/Server-Translations), which makes the server-side functionality of
Enchantment Lore possible!

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT,
however, ~~so anyone else is free to port it.~~ and [tinytransfem](https://www.curseforge.com/members/tinytransfem) has
[ported it to Forge](https://www.curseforge.com/minecraft/mc-mods/enchantment-lore-forge)!

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
