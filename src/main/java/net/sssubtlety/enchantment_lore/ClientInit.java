package net.sssubtlety.enchantment_lore;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ingame.BookScreen;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;

import org.apache.commons.lang3.mutable.MutableInt;

import java.util.ArrayList;
import java.util.List;

import static net.sssubtlety.enchantment_lore.EnchantmentLore.LINES_PER_PAGE;
import static net.sssubtlety.enchantment_lore.mixin.BookScreenAccessor.enchantment_lore$getMAX_TEXT_WIDTH;

public class ClientInit implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EnchantmentLore.setSide(new Side());
        UseItemCallback.EVENT.register(EnchantmentLore::useEnchantedBook);
    }

    public static final class Side implements EnchantmentLore.Side {
        private Side() { }

        @Override
        public boolean isClient() { return true; }

        @Override
        public void openScreen(List<Text> pages, PlayerEntity user) {
            MinecraftClient.getInstance().setScreen(new BookScreen(new BookScreen.Contents(pages)));
        }

        @Override
        public boolean hasTranslation(String key) {
            return I18n.hasTranslation(key);
        }

        @Override
        public List<Text> wrapPages(Text content) {
            final List<Text> pages = new ArrayList<>();

            final MutableInt pageStart = new MutableInt();
            final MutableInt pageLineCount = new MutableInt();

            final var contentString = content.getString();
            MinecraftClient.getInstance().textRenderer.getTextHandler()
                .wrapLines(contentString, enchantment_lore$getMAX_TEXT_WIDTH(), Style.EMPTY, true,
                    (style, lineStart, lineEnd) -> {
                        pageLineCount.increment();

                        if (pageLineCount.getValue() >= LINES_PER_PAGE || lineEnd == contentString.length()) {
                            pages.add(Text.of(contentString.substring(pageStart.getValue(), lineEnd)));
                            pageStart.setValue(lineEnd);
                            pageLineCount.setValue(0);
                        }
                    }
                );

            return pages;
        }

        @Override
        public ActionResult getSuccessResult() {
            return ActionResult.SUCCESS;
        }
    }
}
