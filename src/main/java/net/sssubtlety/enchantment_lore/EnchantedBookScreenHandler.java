package net.sssubtlety.enchantment_lore;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.LecternScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

public final class EnchantedBookScreenHandler extends LecternScreenHandler {
    private static Inventory getBookInventory(ItemStack book) {
        final Inventory inventory = new SimpleInventory(1);
        inventory.setStack(0, book);

        return inventory;
    }

    private EnchantedBookScreenHandler(int syncId, ItemStack book) {
        super(syncId, getBookInventory(book), new ArrayPropertyDelegate(1));
    }

    @Override
    public boolean onButtonClick(PlayerEntity player, int id) {
        if (id == TAKE_BOOK_BUTTON) {
            return false;
        } else {
            return super.onButtonClick(player, id);
        }
    }

    public static class Factory implements NamedScreenHandlerFactory {
        private final ItemStack book;

        public Factory(ItemStack book) {
            this.book = book.copy();
        }

        @Override
        public Text getDisplayName() {
            return ServerInit.ENCHANTED_BOOK;
        }

        @Nullable
        @Override
        public ScreenHandler createMenu(int i, PlayerInventory playerInventory, PlayerEntity playerEntity) {
            return new EnchantedBookScreenHandler(i, this.book);
        }
    }
}
