package net.sssubtlety.enchantment_lore;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.registry.Holder;
import net.minecraft.registry.RegistryKey;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class EnchantmentLore {
    public static final String NAMESPACE = "enchantment_lore";

    private static final String NAMESPACE_DOT = NAMESPACE + ".";
    public static final MutableText CONTAINS_NO_ENCHANTMENTS =
        Text.translatable(NAMESPACE_DOT + "contains_no_enchantments");
    public static final int LINES_PER_PAGE = 14;

    private static Side SIDE;

    public static void setSide(@NotNull Side side) { SIDE = Objects.requireNonNull(side); }

    public static ActionResult useEnchantedBook(PlayerEntity user, World world, Hand hand) {
        final ItemStack usedStack = user.getStackInHand(hand);
        if (SIDE.isClient() != world.isClient || usedStack.getItem() != Items.ENCHANTED_BOOK) {
            return ActionResult.PASS;
        }

        final List<String> storedEnchantmentIds = EnchantmentHelper.getEnchantments(usedStack).getEnchantments()
            .stream()
            .map(Holder::getKey)
            .flatMap(Optional::stream)
            .map(RegistryKey::getValue)
            .map(Object::toString)
            .toList();

        final List<Text> pages = new ArrayList<>();

        if (storedEnchantmentIds.isEmpty()) {
            pages.add(CONTAINS_NO_ENCHANTMENTS);
        } else {
            for (final String id : storedEnchantmentIds) {

                final Optional<Text> description = getTranslation(NAMESPACE_DOT + id + ".description");
                final boolean descriptionPresent = description.isPresent();
                if (descriptionPresent) {
                    pages.addAll(SIDE.wrapPages(description.orElseThrow()));
                }

                final Optional<Text> lore = getTranslation(NAMESPACE_DOT + id + ".lore");
                final boolean lorePresent = lore.isPresent();
                if (lorePresent) {
                    pages.addAll(SIDE.wrapPages(lore.orElseThrow()));
                }

                if (!descriptionPresent && !lorePresent) {
                    pages.add(Text.translatable(NAMESPACE_DOT + "no_translation_for").copy().append(id));
                }
            }
        }

        SIDE.openScreen(pages, user);

        return SIDE.getSuccessResult();
    }

    private static Optional<Text> getTranslation(String key) {
        // TODO:
        //  find server-side equivalent to hasTranslation
        if (SIDE.hasTranslation(key)) {
            final Text translation = Text.translatable(key);
            if (!translation.getString().isEmpty()) {
                return Optional.of(translation);
            }
        }

        return Optional.empty();
    }

    public sealed interface Side permits ClientInit.Side, ServerInit.Side {
        boolean isClient();

        void openScreen(List<Text> pages, PlayerEntity user);

        boolean hasTranslation(String key);

        List<Text> wrapPages(Text content);

        ActionResult getSuccessResult();
    }
}
