package net.sssubtlety.enchantment_lore;

import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.WrittenBookContentComponent;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.message.Filterable;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;

import java.util.ArrayList;
import java.util.List;

import static net.sssubtlety.enchantment_lore.EnchantmentLore.LINES_PER_PAGE;

public class ServerInit implements DedicatedServerModInitializer {
    public static final Text ENCHANTED_BOOK = Text.translatable("item.minecraft.enchanted_book");
    private static final int CHARS_PER_LINE = 15;

    @Override
    public void onInitializeServer() {
        EnchantmentLore.setSide(new Side());
        UseItemCallback.EVENT.register(EnchantmentLore::useEnchantedBook);
    }

    public static final class Side implements EnchantmentLore.Side {
        private Side() { }

        @Override
        public boolean isClient() { return false; }

        @Override
        public void openScreen(List<Text> pages, PlayerEntity user) {
            final var dummyBook = new ItemStack(Items.WRITTEN_BOOK);

            dummyBook.set(DataComponentTypes.WRITTEN_BOOK_CONTENT, new WrittenBookContentComponent(
                Filterable.passthrough(ENCHANTED_BOOK.getString()),
                "supersaiyansubtlety",
                3,
                pages.stream()
                    .map(Filterable::passthrough)
                    .toList(),
                false
            ));

            user.openHandledScreen(new EnchantedBookScreenHandler.Factory(dummyBook));
        }

        @Override
        public boolean hasTranslation(String key) {
            return !Text.translatable(key).getString().equals(key);
        }

        @Override
        public List<Text> wrapPages(Text content) {
            final List<Text> pages = new ArrayList<>();

            final var contentString = content.getString();

            int pageStart = 0;
            int pageLineCount = 0;

            int lineCharCount = 0;
            boolean skipCharCount = false;

            for (int i = 0; i < contentString.length(); i++) {
                final char c = contentString.charAt(i);

                if (skipCharCount) {
                    skipCharCount = false;
                } else {
                    if (c == '§') {
                        skipCharCount = true;
                    } else {
                        final int lineEnd = i + 1;
                        final boolean atEnd = lineEnd == contentString.length();
                        if (atEnd || c == '\n' || lineCharCount >= CHARS_PER_LINE) {
                            pageLineCount++;
                            lineCharCount = 0;

                            if (atEnd || pageLineCount >= LINES_PER_PAGE) {
                                pages.add(Text.of(contentString.substring(pageStart, lineEnd)));
                                pageStart = lineEnd;
                                pageLineCount = 0;
                            }
                        } else {
                            lineCharCount++;
                        }
                    }
                }
            }

            return pages;
        }

        @Override
        public ActionResult getSuccessResult() {
            return ActionResult.SUCCESS_SERVER;
        }
    }
}
