package net.sssubtlety.enchantment_lore.mixin;

import net.minecraft.client.gui.screen.ingame.BookScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(BookScreen.class)
public interface BookScreenAccessor {
    @Accessor("MAX_TEXT_WIDTH")
    static int enchantment_lore$getMAX_TEXT_WIDTH() { throw new UnsupportedOperationException(); }
}
